#
# settings.coffee
#   Configuration file for application settings.
#

path = require "path"

# Defining the node envirnoment (production, development, etc.)
# Dependant on the NODE_ENV variable.
NODE_ENV = global.process.env.NODE_ENV or "development"

# Project name -- ugly but it works.
projectName = (require "../package.json").name

# Project root directory
projectRoot = path.dirname(__dirname)

# Application root
appRoot         = path.join projectRoot, "app"

# Scripts directory
scriptsDirectory = path.join appRoot, "scripts"


# View engine. The default is "jade".
viewEngine      = "jade"

# Temporal directory in case of multiple task over other tasks.
tmpDirectory    = ".tmp"

# Build directory. Default value is ".dist"
buildDirectory  = ".dist"

# Defining the exported object. If you want to modify the values,
# modify them in the variables defined above.
module.exports =
  NODE_ENV        : NODE_ENV
  projectName     : projectName
  projectRoot     : projectRoot
  appRoot         : appRoot
  scriptsDirectory : scriptsDirectory
  viewEngine      : viewEngine
  buildDirectory  : buildDirectory

