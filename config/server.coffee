#
# server.coffee
#   Configuration file for the server.
#
#   Define connection settings for each environment. This
#   environment is defined by the NODE_ENV setting in
#   config/config.coffee.
#

serverSettings =

  development:

    port          : 3000
    hostname      : "http://localhost"
    listenAddress : "localhost"


module.exports = serverSettings
