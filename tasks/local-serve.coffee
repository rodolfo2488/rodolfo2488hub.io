#
# example.coffee:
#   Example of a custom task for Grunt.
#
#   If you want to add a custom task, you may use this as a
#   template.
#

task = (grunt, options) ->

  settings    = require "../config/settings"

  path        = require "path"
  name        = path.basename(__filename, ".coffee")

  description = "Serve the aplication."

  job         = ["connect"]

  grunt.registerTask name, description, job

module.exports = task
