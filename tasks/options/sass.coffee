#
#
# compass.coffee:
#   Example of a option file for a task in Grunt.
#
#   If you want to set options for a task, duplicate this file,
#   with name equal to the task to be configured and change
#   the contents variable.
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  application:
    files:[{
      expand: true
      cwd: path.join(settings.appRoot, "stylesheets")
      src: ["application.sass"]
      dest: path.join(settings.buildDirectory, "stylesheets")
      ext: ".min.css"
    }]

module.exports = contents
