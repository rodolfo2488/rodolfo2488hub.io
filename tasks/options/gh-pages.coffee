#
#
# gh-pages.coffee:
#   push to gh-pages
#

contents = (grunt, options) ->

  settings = require "../../config/settings"

  options:
    base: settings.buildDirectory
    branch: "master"
  src: ["**/*"]

module.exports = contents
