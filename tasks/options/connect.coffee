#
#
# example.coffee:
#   Example of a option file for a task in Grunt.
#
#   If you want to set options for a task, duplicate this file,
#   with name equal to the task to be configured and change
#   the contents variable.
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  server   = require "../../config/server"

  server:
    options:
      base: settings.buildDirectory
      port: server.development.port
      protocol: 'http'
      livereload: true

module.exports = contents
