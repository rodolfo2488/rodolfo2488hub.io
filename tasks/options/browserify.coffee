#
#
# example.coffee:
#   Example of a option file for a task in Grunt.
#
#   If you want to set options for a task, duplicate this file,
#   with name equal to the task to be configured and change
#   the contents variable.
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  transformers = [
    "coffeeify",
    "browserify-ngannotate",
    "browserify-plain-jade"
     # Uncomment these lines to enable transformers for usage with other
     # front-end package managers and solutions.
     # "decomponentify"
     # "deamdify"
     # "deglobalify"
     # "es6ify"
  ]

  dist:
    src: path.join settings.scriptsDirectory, "app.coffee"
    dest: path.join settings.buildDirectory, "scripts", "app.js"
    options:
      transform: transformers
      extensions: [".coffee", ".js"]
    browserifyOptions:
      debug: true

module.exports = contents
