#
#
# cssmin.coffee:
#

contents = (grunt, options) ->

  settings = require "../../config/settings"
  path     = require "path"

  angular_material:
    files: [{
      expand: true
      cwd: "node_modules/angular-material"
      src: ["**/*.css"]
      dest: path.join(settings.buildDirectory, "vendor", "stylesheets")
      ext: ".min.css"
    }]

module.exports = contents
