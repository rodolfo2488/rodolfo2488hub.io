#
# pkg.coffee:
#   Package settings for Grunt.
#

contents = (grunt, options) ->

  settings = require "../../config/settings"

  grunt.file.readJSON("package.json")

module.exports = contents