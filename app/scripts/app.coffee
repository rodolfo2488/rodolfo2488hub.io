"use strict"

window.Hammer = require "hammerjs/hammer"

require "angular/angular"
require "angular-animate/angular-animate"
require "angular-aria/angular-aria"
require "angular-material/angular-material"
require "ui-router"

profile = require "./modules/profile"

angular.module("rodolfo2488",[
  "ngMaterial",
  "ui.router",
  profile.name
])
.config( ($mdThemingProvider) ->
  $mdThemingProvider.alwaysWatchTheme(true)
)
