'use strict'

controller = require "./controller.coffee"

module.exports = angular.module('rodolfo2488.example', [
  "ngMaterial"
])
.controller('ExampleController', controller)
