rodolfo2488.github.io
=====================

http://rodolfo2488.github.io


Simple one-page application using Angular and Grunt for gh-pages

#Grunt Tasks

###push

It will build your application in the `.dist` folder and push that to your `master` branch in your gh-page.

You should have in your `package.json`:

```json
repository": {
    "type": "git",
    "url": "https://github.com/{{githubUsername}}/{{githubUsername}}.io.git"
  },
```

For more usage see documentation for `grunt-gh-pages`


###local-serve

It will connect the application locally in the **port** and **base** in your `config/server.coffee`

###example

It is a simple example how to build customs tasks for grunt using **module.exports** notation in CoffeeScript.

#Grunt Options

All the grunt options are in the `tasks/options` folder for setting up your grunt environment.

###alias.yaml

it is for an easy way to build tasks that does not need any extra configuration.

###example.coffee

It is an example of how to create a grunt option using **module.exports** notation.

#Angular Part

For Angular is build as the same notation using **module.exports** in the `apps/scripts/modules`  should be all the sub-applications and controllers of the main app.

###example module

it is an example of a really simple module of angular.

if you want to add another angular library like **angular-route** please use **napa** to upload them in the `install` of npm.

An example how to do it is to add in **napa** object in **package.json** this:

```"angular-route": "angular/bower-angular-route"```

Then you can require it in `app.coffee` using 

```require "angular/angular-route"```


## Contributing

I would love getting feedback with or without pull requests. If you do add a new
feature, please add tests so that we can avoid breaking it in the future.

## License
[MIT License](http://en.wikipedia.org/wiki/MIT_License)
